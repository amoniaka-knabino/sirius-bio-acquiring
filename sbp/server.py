from flask import jsonify, Flask
import random
app = Flask(__name__)


@app.route('/get_ok', methods=['GET', 'POST'])
def get_ok():
    if random.randint(1, 10) <= 2:
        return jsonify({
            'error': 'Недостаточно средств'
        }), 418
    return jsonify({'result': 'ok'})


if __name__ == "__main__":
    app.run(port=6230, host='0.0.0.0')
