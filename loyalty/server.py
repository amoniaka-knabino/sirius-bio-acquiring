from flask import jsonify, Flask, request
from db_module import (
    check_if_exists_by_ebs_id,
    add_client_to_db, session_factory,
    get_persinfo_dict_by_ebs_id
)
from helpers import (
    get_error_json_dict,
    user_already_exists_message,
    user_not_found_message,
)

app = Flask(__name__)


@app.route('/register', methods=["POST"])
def register():
    session = session_factory()
    ebs_id = request.json.get('ebs_id')
    name = request.json.get('name')
    surname = request.json.get('surname')
    patronymic = request.json.get('patronymic')
    email = request.json.get('email')
    birth_day = request.json.get('birthday')[0]
    birth_month = request.json.get('birthday')[1]
    birth_year = request.json.get('birthday')[2]
    if check_if_exists_by_ebs_id(session, ebs_id):
        return jsonify(
            get_error_json_dict(user_already_exists_message)
            ), 400
    lid = add_client_to_db(
        session,
        ebs_id, name, surname, patronymic, email,
        birth_day, birth_month, birth_year
    )
    return jsonify({'loyalty_id': lid})


@app.route('/get_user_by_id', methods=["POST"])
def get_user_by_id():
    session = session_factory()
    ebs_id = request.json.get('ebs_id')
    person = get_persinfo_dict_by_ebs_id(session, ebs_id)
    if person:
        return jsonify(
            person
        ), 200
    else:
        return jsonify(
            get_error_json_dict(user_not_found_message)
        ), 404


if __name__ == "__main__":
    app.run(port=6229, host='0.0.0.0')
