from sqlalchemy import create_engine, Column, BigInteger, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
import random

from sqlalchemy.sql.sqltypes import Integer

DB_HOST = 'postgres2'
DB_NAME = os.getenv('POSTGRES_DB')
DB_USER = os.getenv('POSTGRES_USER')
DB_PASSWORD = os.getenv('POSTGRES_PASSWORD')

engine = create_engine((f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"), echo=True)
# use session_factory() to get a new Session
_SessionFactory = sessionmaker(bind=engine)

Base = declarative_base()


def generate_id():
    return random.randint(10**10, 10**11-1)


def session_factory():
    Base.metadata.create_all(engine)
    return _SessionFactory()


class Client(Base):
    __tablename__ = 'client'

    loyalty_id = Column(BigInteger, primary_key=True)
    ebs_id = Column(BigInteger)
    sale_size = Column(Integer)
    name = Column(String)
    surname = Column(String)
    patronymic = Column(String)
    email = Column(String)
    birth_day = Column(Integer)
    birth_month = Column(Integer)
    birth_year = Column(Integer)

    def __init__(
        self, ebs_id, name, surname, patronymic, email,
        birth_day, birth_month, birth_year
    ):
        self.loyalty_id = generate_id()
        self.ebs_id = ebs_id
        self.name = name
        self.surname = surname
        self.patronymic = patronymic
        self.email = email
        self.birth_day = birth_day
        self.birth_month = birth_month
        self.birth_year = birth_year
        self.sale_size = 20


def get_persinfo_dict_by_ebs_id(session, ebs_id):
    clients = session.query(Client).where(Client.ebs_id == ebs_id).all()
    print(clients)
    if clients:
        c = clients[0]
        return {
            "birthday": [
                int(c.birth_day),
                int(c.birth_month),
                int(c.birth_year)
            ],
            "email": str(c.email),
            "name": str(c.name),
            "patronymic": str(c.patronymic),
            "surname": str(c.surname),
            "ebs_id": str(c.ebs_id),
            "sale_size": int(c.sale_size)
        }
    return None


def check_if_exists_by_ebs_id(session, ebs_id):
    clients = session.query(Client).where(Client.ebs_id == ebs_id).all()
    print(clients)
    if len(clients):
        return True
    return False


def add_client_to_db(
    session,
    ebs_id, name, surname, patronymic, email,
    birth_day, birth_month, birth_year
):
    c = Client(
        ebs_id, name, surname, patronymic, email,
        birth_day, birth_month, birth_year
    )
    session.add(c)
    session.commit()
    return c.loyalty_id


def get_by_id(session, ebs_id):
    clients = session.query(Client).where(Client.ebs_id == ebs_id)
    if clients:
        c = clients[0]
        return (
            c.ebs_id, c.name, c.surname, c.patronymic, c.email,
            c.birth_day, c.birth_month, c.birth_year
            )
    return None
