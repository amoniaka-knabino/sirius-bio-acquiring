from flask import Flask, jsonify, request
from db_module import (
    test,
    session_factory, 
    get_user_id_by_vector,
    add_new_user_and_get_id
)
from helpers import (
    get_error_json_dict, get_person_json_dict,
    UserNotFound, user_not_found_message,
    user_already_exists_message
)

app = Flask(__name__)


@app.route('/get_user_id', methods=["POST"])
def get_user():
    session = session_factory()
    vector = request.json.get('vector')
    print(vector)
    try:
        id = get_user_id_by_vector(session, vector)
        return jsonify(
            get_person_json_dict(id)
        ), 200
    except UserNotFound:
        return jsonify(
            get_error_json_dict(
                user_not_found_message
            )
        ), 404
    finally:
        session.commit()
        session.close()


@app.route('/register', methods=["POST"])
def register():
    session = session_factory()
    vector = request.json.get('vector')
    print(vector)
    try:
        get_user_id_by_vector(session, vector)
        return jsonify(
            get_error_json_dict(user_already_exists_message)
        ), 400
    except UserNotFound:
        print('register')
        id = add_new_user_and_get_id(session, vector),
        return jsonify(
            get_person_json_dict(id)
        )
    finally:
        session.commit()
        session.close()


@app.route('/test')
def test_db():
    p = test()
    return jsonify({'id':str(p[0]), 'vec': str(p[1])})

if __name__ == "__main__":
    app.run(port=6226, host='0.0.0.0')