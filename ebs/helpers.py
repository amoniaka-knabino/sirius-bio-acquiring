import numpy as np

user_already_exists_message = (
    "Данный пользователь уже зарегистрирован в базе данных"
)

user_not_found_message = (
    "Данный пользователь не найден в базе данных"
)

MIN_DISTANCE = 0.4


class UserAlreadyExists(Exception):
    pass


class UserNotFound(Exception):
    pass


def vector_to_str(vector):
    ans = '['
    for x in vector:
        ans += f'{x}, '
    ans = ans[:-2] + ']'
    return ans


def str_to_vector(vector_str):
    nums = [float(x) for x in vector_str[1:-1].split(', ')]
    return nums


def check_if_vectors_are_equal(vector, other_vector):
    dist = _get_vectors_distance(vector, other_vector)
    return dist < MIN_DISTANCE


def _get_vectors_distance(vector, other_vector):
    if len(vector) == 0 or len(other_vector) == 0:
        return np.empty((0))
    return np.linalg.norm(np.array(vector) - np.array(other_vector), axis=0)


def get_person_json_dict(id):
    return {
        "id": id,
    }


def get_error_json_dict(err_message):
    return {
        "error": err_message
    }
