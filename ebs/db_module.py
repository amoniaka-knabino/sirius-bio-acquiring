from sqlalchemy import create_engine, Column, BigInteger, ARRAY, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os
from helpers import (
    UserAlreadyExists,
    user_already_exists_message,
    UserNotFound,
    user_not_found_message,
    MIN_DISTANCE,
    check_if_vectors_are_equal,
    )
import random

DB_HOST = 'postgres2'
DB_NAME = os.getenv('POSTGRES_DB')
DB_USER = os.getenv('POSTGRES_USER')
DB_PASSWORD = os.getenv('POSTGRES_PASSWORD')

engine = create_engine((f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"), echo=True)
# use session_factory() to get a new Session
_SessionFactory = sessionmaker(bind=engine)

Base = declarative_base()

def generate_id():
    return random.randint(10**10, 10**11-1)

def session_factory():
    Base.metadata.create_all(engine)
    return _SessionFactory()


class Person(Base):
    __tablename__ = 'person'

    id = Column(BigInteger, primary_key=True)
    vector = Column(ARRAY(Float))

    def __init__(self, vector):
        self.id = generate_id()
        self.vector = vector


def check_and_get_if_user_exists(session, vector):
    print('check if exists')
    people_query = session.query(Person)
    session.close()
    people_all = people_query.all()
    for p in people_all:
        other_vector = p.vector
        unknown_vector = vector
        print(f'compare {unknown_vector} and {other_vector}')
        if check_if_vectors_are_equal(unknown_vector, other_vector):
            print('they are equal')
            return p
    return None


def add_new_user_and_get_id(session, vector):
    new_person = Person(vector)
    session.add(new_person)
    return new_person.id


def get_user_id_by_vector(session, vector):
    person = check_and_get_if_user_exists(session, vector)
    if person:
        return person.id
    raise UserNotFound


def test():
    ar = [-0.15812948,  0.10130663,  0.06463128, -0.01819114, -0.08174579,
       -0.033893  ,  0.05704639, -0.06316003,  0.1459257 , -0.08169807,
        0.27148411, -0.10572266, -0.1925364 , -0.09645157,  0.03616079,
        0.08510702, -0.06513101, -0.08729485, -0.06762569, -0.04157277,
        0.04814804,  0.06915552, -0.04191685,  0.11195061, -0.15560308,
       -0.30896884, -0.05963188, -0.06771748, -0.01732537, -0.10507502,
       -0.00315572,  0.14365257, -0.08843765, -0.04325915,  0.05126917,
        0.12138229, -0.07512681,  0.03571463,  0.21531154, -0.02368077,
       -0.21711294, -0.02643707,  0.07438353,  0.32921046,  0.19360611,
        0.05476526,  0.00844687, -0.05365077,  0.18505496, -0.19746602,
        0.03147075,  0.14836313,  0.14485061,  0.04199275,  0.07159755,
       -0.13432035, -0.04208526,  0.15163852, -0.18688519,  0.09156509,
        0.06509583, -0.19507834,  0.00167891, -0.07125737,  0.16333257,
        0.03852174, -0.11506055, -0.12463076,  0.14979708, -0.18322307,
       -0.02043545,  0.24003403, -0.1024537 , -0.23301241, -0.17863196,
        0.08853254,  0.45716295,  0.13289715, -0.10898738, -0.0237476 ,
       -0.01115528, -0.07558171,  0.07302266,  0.10737371, -0.05357587,
       -0.0755803 , -0.10770081,  0.03023278,  0.24870712,  0.00591786,
        0.00865482,  0.20797414,  0.00644193,  0.01648043, -0.01438977,
       -0.01798587, -0.09738464,  0.06628661, -0.03161426, -0.00802181,
       -0.00283767, -0.16287003,  0.04374145,  0.0434384 , -0.19711435,
        0.23007141, -0.02770389,  0.06045803, -0.0184885 , -0.01017823,
       -0.0445579 ,  0.03812114,  0.16220812, -0.27381718,  0.13498467,
        0.07111546,  0.04391362,  0.16757102,  0.01390821,  0.10493853,
        0.01608813, -0.06122055, -0.14288667, -0.04065258,  0.09371383,
       -0.066963  ,  0.13992919, -0.0263657 ]
    session = session_factory()
    bruno_id = add_new_user_and_get_id(session, ar)
    print(bruno_id)
    session.commit()

    ar2 = [-0.15912948,  0.10130663,  0.06463128, -0.01819114, -0.08174579,
       -0.033893  ,  0.05704639, -0.06316003,  0.1459257 , -0.08169807,
        0.27148411, -0.10572266, -0.1925364 , -0.09645157,  0.03616079,
        0.08510702, -0.06513101, -0.08729485, -0.06762569, -0.04157277,
        0.04814804,  0.06915552, -0.04191685,  0.11195061, -0.15560308,
       -0.30896884, -0.05963188, -0.06771748, -0.01732537, -0.10507502,
       -0.00215572,  0.14365257, -0.08843765, -0.04325915,  0.05126917,
        0.12138229, -0.07512681,  0.03571463,  0.21531154, -0.02368077,
       -0.21711294, -0.02643707,  0.07438353,  0.32921046,  0.19360611,
        0.05476526,  0.00844687, -0.05365077,  0.18505496, -0.19746602,
        0.03147075,  0.14836313,  0.14485061,  0.04199275,  0.07159755,
       -0.13432035, -0.03208526,  0.15163852, -0.18688519,  0.09156509,
        0.06509583, -0.19507834,  0.00167891, -0.07125737,  0.16333257,
        0.03852174, -0.11506055, -0.12463076,  0.14979708, -0.18322307,
       -0.02043545,  0.24003403, -0.1024537 , -0.23301241, -0.17863196,
        0.08853254,  0.45716295,  0.13289715, -0.10898738, -0.0237476 ,
       -0.01115528, -0.07558171,  0.07302266,  0.10737371, -0.05357587,
       -0.0755803 , -0.10770081,  0.03023278,  0.24870712,  0.00591786,
        0.00865482,  0.20797414,  0.00644193,  0.01648043, -0.01438977,
       -0.01798587, -0.09738464,  0.06628661, -0.03161426, -0.00802181,
       -0.00283767, -0.16287003,  0.04374145,  0.0434384 , -0.19711435,
        0.23007141, -0.02770389,  0.06045803, -0.0184885 , -0.01017823,
       -0.0445579 ,  0.03812114,  0.16220812, -0.27381718,  0.13498467,
        0.07111546,  0.04391362,  0.16757102,  0.01390821,  0.10493853,
        0.01608813, -0.06122055, -0.14288667, -0.04065258,  0.09371383,
       -0.066963  ,  0.13992919, -0.0263657 ]
    p = check_and_get_if_user_exists(session, ar2)
    session.close()
    return p.id, p.vector

