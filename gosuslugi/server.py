from russian_names import RussianNames
import random
from flask import jsonify, Flask
app = Flask(__name__)

 
def random_email():
   random_value = random.randrange(1, 10)
   mail_server_list = ['gmail.com', 'yandex.ru', 'outlook.com', 'mail.ru']
   random_mail = random.sample('abcdefghijklmnopqrstuvwxyz0123456789', random_value)
   random_mail = ''.join(random_mail)
   random_mail_server = mail_server_list[random.randrange(0, len(mail_server_list))]
   random_mail = random_mail + '@' + random_mail_server
   return random_mail
 
def random_birthday():
  random_date = random.randrange(1, 31)
  random_month = random.randrange(1, 12)
  random_year = random.randrange(1910, 2021)
  return (random_date, random_month, random_year)
 
@app.route('/get_random')
def get_information_by_id():
  user_profile = dict()
  fio = RussianNames().get_person().split()
  user_profile['name'] = fio[0]
  user_profile['surname'] = fio[1]
  user_profile['patronymic'] = fio[2]
  user_profile['email'] = random_email()
  user_profile['birthday'] = random_birthday()
  return jsonify(user_profile)

if __name__ == "__main__":
    app.run(port=6228, host='0.0.0.0')